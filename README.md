# OSM Hackfest 11 - PowerDNS CNF Onboarding

This repository contains the PowerDNS network service as a package for OSM.  

Currently, there are two versions of the package, with different feature sets:
- Helm based OSM package, **contains no primitives support** (i.e., only deployment)
- JuJu based OSM package, **full support for primitives** (day-1, day-2)

For more information about each of the packages refer to their included README files.