#!/usr/bin/env bash
set -o errexit
set -o nounset
#set -o xtrace

if [ $# -ne 1 ]; then
    echo "Usage: $0 <vim>"
    echo "Declare your VIM name (can be retrieved by 'osm vim-list')"
    exit 1
fi

VIM=$1

osm nfpkg-create ./powerdns_cnf
osm nspkg-create ./powerdns_cnf_ns/
osm ns-create \
  --ns_name powerdns-cnf-ns \
  --nsd_name powerdns_cnf_ns \
  --vim_account "${VIM}" \
  --config '{vld: [ {name: mgmtnet, vim-network-name: osm-ext} ] }'
