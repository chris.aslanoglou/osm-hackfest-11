# PowerDNS

## Overview

This is a Kubernetes Charm to deploy [PowerDNS](https://www.powerdns.com/).

Available actions for this charm:
* Create DNS zones  
  `juju run-action powerdns/0 add-zone zone-name=example.org.`
* Add DNS address record (Domain-IP mapping)  
  `juju run-action powerdns/0 add-domain zone-name=example.org. subdomain=test. ip=192.168.1.254`
* Delete DNS zones  
  `juju run-action powerdns/0 delete-zone zone-name=example.org.`
* Delete a domain-IP mapping  
  `juju run-action powerdns/0 delete-domain zone-name=example.org. subdomain=test.`

## Setup Juju & MicroK8s

If you don't have MicroK8s and Juju installed executing the following commands:

```bash
sudo snap install juju --classic
sudo snap install microk8s --classic
juju bootstrap microk8s
```

# Building & deploying

```bash
git clone https://gitlab.ubitech.eu/nsit/nfv/osm/hackfest-11-powerdns.git
cd hackfest-11-powerdns/juju-cnf/powerdns_cnf/juju-bundles
./build-charm.sh

juju add-model powerdns
juju deploy --verbose /path/to/bundle.yaml
```
