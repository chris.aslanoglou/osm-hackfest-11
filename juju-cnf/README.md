# Juju bundle for PowerDNS

Developed for the purposes of `OSM-Hackfest-11` by Christos Aslanoglou,
Vasileios Nikolaos Apostolopoulos and Georgios Katsikas.

This Juju bundle uses a PowerDNS image from `vapostolopoulos/powerdns` 
dockerhub repo. It is basically `psitrax/powerdns:4.4.1` image with python3
and py3-pip installed.

# Day 1-2 OSM primitives
See `initial-config-primitive` in `powerdns_vnfd.yaml` for details.
```
add-zone
add-domain

delete-zone
delete-domain
```

# Build the charm 
```
cd path/to/hackfest-11/juju-cnf/powerdns_cnf/juju-bundles
./build-charm.sh
```

# Deploy bundle to OSM
```
cd path/to/hackfest-11/juju-cnf
./deploy-ns.sh your-vim-name
```

# Test deployment
```
$ K8S_NAMESPACE=$(osm ns-show powerdns-cnf-ns --literal | grep kdu-instance | awk '{ print $2 }')
$ LOADBALANCER_IP=$(kubectl -n ${K8S_NAMESPACE} get svc powerdns -o json | jq ".status.loadBalancer.ingress[0].ip" -r)
$ dig @${LOADBALANCER_IP} +tcp test.example.org
; <<>> DiG 9.11.3-1ubuntu1.15-Ubuntu <<>> @172.16.0.21 +tcp test.example.org
; (1 server found)
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 25399
;; flags: qr aa rd; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1
;; WARNING: recursion requested but not available

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
;; QUESTION SECTION:
;test.example.org.		IN	A

;; ANSWER SECTION:
test.example.org.	86400	IN	A	192.0.5.4  ;; <--- You should this IP 

;; Query time: 2 msec
;; SERVER: 172.16.0.21#53(172.16.0.21)
;; WHEN: Thu Jun 17 13:58:03 UTC 2021
;; MSG SIZE  rcvd: 61

```